'use strict';

// var a = 1;
// var b = 100;

// for (var i = a; i <= b; i++) {
//
//     if (i % 3 === 0 && i % 5 === 0) {
//         console.log(i + 'Fuddy-Duddy');
//         continue;
//     }
//
//     if (i % 3 === 0) {
//         console.log(i + 'Fuddy');
//         continue;
//     }
//
//     if (i % 5 === 0) {
//         console.log(i + 'Duddy');
//         continue;
//     }
//
//     console.log(i);
// }

function fuddyDuddy (a, b) {

    for (var i = a; i <= b; i++) {

        if (i % 3 === 0 && i % 5 === 0) {
            console.log(i + 'Fuddy-Duddy');
            continue;
        }

        if (i % 3 === 0) {
            console.log(i + ' Fuddy');
            continue;
        }

        if (i % 5 === 0) {
            console.log(i + ' Duddy');
            continue;
        }

        console.log(i);
    }
}

fuddyDuddy(1, 100);
console.log('--------------------------');
